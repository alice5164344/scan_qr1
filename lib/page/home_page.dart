import 'package:flutter/material.dart';
import 'package:scan_qr/page/profile_page.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Home Page",
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: Color(0x8E1739),
      ),
      body: Column(
        children: [
          Text("Hello Word"),
          ElevatedButton(
            onPressed: () {
              Navigator.push(context,
              MaterialPageRoute(builder: (BuildContext ctx)=>ProfilePage()));
            },
            child: Text("Simpan"),
            style: ElevatedButton.styleFrom(
                backgroundColor: Colors.red
            ),
          ),
        ],
      ),
    );
  }
}