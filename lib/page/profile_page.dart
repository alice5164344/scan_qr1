import 'package:flutter/material.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({super.key});

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar (
        backgroundColor: Colors.blue,
        title: Text("Profile Page",
            style: TextStyle (
              color: Colors.white,
            )),
      ),
      body: Column (
        children: [
          customForm(
              icon: Icon(Icons.person),
              hintText: "Masukkan Nama"
          ),
          customForm(
              icon: Icon(Icons.mail),
              hintText: "Masukkan Email"
          ),
          ElevatedButton(
            onPressed: () {},
            child: Text("Simpan"),
            style: ElevatedButton.styleFrom(
                backgroundColor: Colors.red
            ),
          ),
        ],
      ),
    );
  }

  Widget customForm({required Icon icon, required String hintText}) {
    return Padding(
          padding: const EdgeInsets.all(8.0),
          child: TextFormField(
            decoration: InputDecoration(
                hintText: "Nama Sesuai KTP",
                prefixIcon: icon,
                suffixIcon: Icon(Icons.chevron_right),
                border: OutlineInputBorder(
                    borderSide: BorderSide(
                      width: 1,
                      color: Colors.blueAccent,
                    ))),
          ),
        );
  }
}
